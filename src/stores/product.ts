import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "@/stores/message";
import { useConfirmStore } from "./confirm";

export const useProductsStore = defineStore("product", () => {
  const confirm = useConfirmStore();
  const products = ref<Product[]>([]);
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProduct();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Connect Server");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;

      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Save Product");
    }
    loadingStore.isLoading = false;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Delete Product");
    }
    loadingStore.isLoading = false;
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    editProduct,
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    deleteProduct,
  };
});
