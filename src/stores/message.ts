import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");

  function showError(msg: string) {
    message.value = msg;
    isShow.value = true;
  }

  return { isShow, message, showError };
});
