import { ref } from "vue";
import { defineStore } from "pinia";
import { useProductsStore } from "./product";
import { useUserStore } from "./user";

export const useConfirmStore = defineStore("confirm", () => {
  const isShow = ref(false);
  const product = useProductsStore();
  const user = useUserStore();
  const Id = ref<number>();
  const OnService = ref();
  function DelProduct(id: number) {
    switch (OnService.value) {
      case "product":
        if (Id.value !== -1) {
          product.deleteProduct(id);
          Id.value = -1;
          isShow.value = false;
        }
        break;
      case "user":
        if (Id.value !== -1) {
          user.deleteUser(id);
          Id.value = -1;
          isShow.value = false;
        }
        break;
    }
  }
  function OnConfirm(id: number, service: string) {
    isShow.value = true;
    Id.value = id;
    OnService.value = service;
  }

  return { isShow, DelProduct, OnConfirm, Id };
});
