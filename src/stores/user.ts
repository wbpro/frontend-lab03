import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import UserService from "../services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("user", () => {
  const user = ref<User[]>([]);
  const loading = useLoadingStore();
  const editedUser = ref<User>({ login: "", name: "", password: "" });
  const dialog = ref(false);
  const messageStore = useMessageStore();

  async function getUser() {
    try {
      loading.isLoading = true;
      const res = await UserService.getUser();
      user.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Connect Server");
    }
    loading.isLoading = false;
  }

  async function saveUser() {
    try {
      loading.isLoading = true;
      if (editedUser.value.id) {
        await UserService.updateUser(editedUser.value.id, editedUser.value);
      } else {
        await UserService.saveUser(editedUser.value);
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Save User");
    }
    await getUser();
    loading.isLoading = false;
    dialog.value = false;
  }
  async function deleteUser(id: number) {
    loading.isLoading = true;
    try {
      const res = await UserService.deleteUser(id);
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("Can't Delete User");
    }
    loading.isLoading = false;
  }

  function editUser(User: User) {
    editedUser.value = JSON.parse(JSON.stringify(User));
    dialog.value = true;
  }

  return { user, getUser, saveUser, dialog, editedUser, editUser, deleteUser };
});
