import type User from "@/types/User";
import http from "./axios";

function getUser() {
  return http.get("/users");
}
function saveUser(User: User) {
  return http.post("/users", User);
}
function updateUser(id: number, User: User) {
  return http.patch(`/users/${id}`, User);
}
function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}
export default { getUser, saveUser, updateUser, deleteUser };
